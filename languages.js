document.querySelector('select').addEventListener('change', function(){
    chrome.storage.local.set({"lang": document.querySelector('select option:checked').value},null);
});

chrome.storage.local.get(['lang'], function(result){
    document.querySelector('select option[value="'+result.lang+'"]').setAttribute('selected', 'selected');
});