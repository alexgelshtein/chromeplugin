function readText(k){
    if((k.key.toLowerCase() === 's' || k.key.toLowerCase() === 'ы') && k.shiftKey){
        var str = window.getSelection().anchorNode.data;
        var text = str.toString().substring(window.getSelection().focusOffset, window.getSelection().anchorOffset);
        chrome.storage.local.get(['lang'], function(result){
            var lang = 'en-US';
            if(Object.keys(result).length !== 0){
                lang = result.lang;
            }
            var speech = window.speechSynthesis,
                message = new SpeechSynthesisUtterance();
                message.lang = lang;
                message.text = text;
                speech.speak(message);
        });
    }
}

window.onkeydown = readText;